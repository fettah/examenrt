package com.example.examaujou;

import org.springframework.batch.item.ItemProcessor;

public class DataValidationProcessor implements ItemProcessor< MyData, MyData> {
    @Override
    public MyData process(MyData item) {
        if (isValidData(item)) {
            return item;
        } else {
            return null;
        }
    }

    private boolean isValidData(MyData item) {
        try {

            Integer.parseInt(item.colonne1());


            return true;
        } catch (NumberFormatException e) {

            return false;
        }
    }
}
