package com.example.examaujou;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.step.builder.StepBuilder;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.builder.FlatFileItemWriterBuilder;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jdbc.support.JdbcTransactionManager;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

@Configuration
@EnableBatchProcessing
public class JobConfiguration {


    @Bean

    public FlatFileItemReader<MyData> getFlatFileItemReader() {
        return new FlatFileItemReaderBuilder<MyData>()
                .name("myDataItemReader")
                .resource(new FileSystemResource("Output/output.csv"))
                .linesToSkip(calculateLinesToSkip("Output/output.csv"))
                .delimited()
                .names("colonne1", "colonne2") // Remplacez par les noms de vos colonnes
                .targetType(MyData.class)
                .build();
    }

    private int calculateLinesToSkip(String filePath) {
        try (BufferedReader reader = new BufferedReader(new FileReader("Output/output.csv"))) {
            long totalLines = reader.lines().count();
            return (int) (totalLines * 0.05); // 5% des données à sauter
        } catch (IOException e) {
            e.printStackTrace();
            return 0;
        }
    }



    @Bean
    public FlatFileItemWriter<MyData> getFlatFileItemWriter(
            @Value("#{jobParameters['output.file']}") String outputFile) {
        return new FlatFileItemWriterBuilder<MyData>()
                .name("getFlatFileItemWriter")
                .resource(new FileSystemResource(outputFile))
                .lineAggregator(new DelimitedLineAggregator<MyData>() {{
                    setDelimiter(",");
                    setFieldExtractor(new BeanWrapperFieldExtractor<MyData>() {{
                        setNames(new String[]{"colonne1", "colonne2"}); // Remplacez par les noms de vos colonnes
                    }});
                }})
                .build();
    }
    @Bean
    public DataValidationProcessor myDataProcessor() {
        return new DataValidationProcessor();
    }

    @Bean
    public Job myJob(Step step1 , JobRepository jobRepository) {
        return new JobBuilder("my job",jobRepository)
                .start(step1)
                .build();
    }

    @Bean
    public Step myStep(FlatFileItemReader<MyData> myDataReader, JobRepository jobRepository , JdbcTransactionManager transactionManager,
                       ItemProcessor<MyData,MyData> myDataProcessor, FlatFileItemWriter<MyData> getFlatFileItemWriter) {

        return new StepBuilder("fileIngestion",jobRepository)
                .<MyData,MyData>chunk(100,transactionManager)
                .reader(myDataReader)
                .processor(myDataProcessor)
                .faultTolerant()
                .retryLimit(3)
                .writer(getFlatFileItemWriter)
                .build();

    }

}


