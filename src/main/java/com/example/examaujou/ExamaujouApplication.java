package com.example.examaujou;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExamaujouApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExamaujouApplication.class, args);
    }

}
