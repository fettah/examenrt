
UPDATE Person.Address
SET AddressID = 'ValeurNonValide'
WHERE RAND() < 0.05 AND AddressID IS NOT NULL;

UPDATE Person.AddressType
SET AddressTypeID = CAST(AddressTypeID AS NVARCHAR(MAX))
WHERE RAND() < 0.1 AND AddressTypeID IS NOT NULL;
